echo 'current dir is '
pwd
unset PYTHONPATH
export PATH=/usr/local/bin:/usr/bin:/bin:/opt/oar/utilities:/sbin:/usr/bin/X11:.::.
unset LDFLAGS
unset CPPFLAGS
unset LD_LIBRARY_PATH

rm -rf /tmp/software/chst_python3
virtualenv --python=python3 /tmp/software/chst_python3
source /tmp/software/chst_python3/bin/activate
pip install setuptools --upgrade
pip install pip --upgrade
pip install numpy
pip install -r requirements.txt
python setup.py install
python run_tests.py -v --testcase oargpu
