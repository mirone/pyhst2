import string

def six_atoi(s):
    return int(s)

def six_atof(s):
    return float(s)

def six_lower(s):
    return s.lower()

def six_upper(s):
    return s.upper()

def six_replace(s,a,b):
    return s.replace(a,b)

def six_count(s,a):
    print(" COUNT ", s ,  " == " , a ) 
    return s.count(a)


string.atoi = six_atoi
string.atof = six_atof
string.lower = six_lower
string.upper = six_upper
string.replace = six_replace
string.count = six_count

