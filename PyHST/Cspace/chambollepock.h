

typedef struct ParamsForTomo {
  Gpu_Context *ctxstruct;
  float DETECTOR_DUTY_RATIO;
  int DETECTOR_DUTY_OVERSAMPLING;
} ParamsForTomo ;

#ifdef __cplusplus
extern "C" {
  int chambolle_pock_driver(Gpu_Context* self, float* data, float* SLICE, float DETECTOR_DUTY_RATIO, int DETECTOR_DUTY_OVERSAMPLING);
  float calculate_lipschitz(ParamsForTomo p4t, float* sino, float *slice, int n_it);
}
#else
int chambolle_pock_driver(Gpu_Context* self, float* data, float* SLICE, float DETECTOR_DUTY_RATIO, int DETECTOR_DUTY_OVERSAMPLING);
float calculate_lipschitz(ParamsForTomo p4t, float* sino, float *slice, int n_it);

#endif
