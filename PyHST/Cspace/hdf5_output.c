#define H5Dcreate_vers 1
#define H5Gcreate_vers 1
#define H5Dopen_vers   1
#include <hdf5.h>
#include<libgen.h>
#include<string.h>

#define DATASETNAME "ExtendibleArray" 

void add_float(hid_t group, char * name, float value)  {
  herr_t      status;

  hid_t space = H5Screate (H5S_SCALAR);
  hid_t dset =  H5Dcreate (group , name, H5T_NATIVE_FLOAT, space, H5P_DEFAULT);;

  status = H5Dwrite(dset,
		    H5T_NATIVE_FLOAT ,
		    H5S_ALL,
		    H5S_ALL,
		    H5P_DEFAULT, &value);
  
  status = H5Dclose( dset ) ; 
  status = H5Sclose( space ) ; 
  
}  


void add_attribute( hid_t location_id,    char *attr_name, char *attr_value) {
  hid_t         memtype, space, dset, attr;
  /* Handles */
  herr_t      status;
  hsize_t     dims[1] = {1};
  size_t      sdim;
  
  memtype = H5Tcopy (H5T_C_S1);
  status = H5Tset_size (memtype, strlen(attr_value )  );
  
  space = H5Screate (H5S_NULL);
  dset = H5Dcreate (location_id, attr_name, memtype, space, H5P_DEFAULT);
  
  status = H5Sclose (space);

  // space = H5Screate_simple (1, dims, NULL);
  space = H5Screate (H5S_SCALAR);

  attr = H5Acreate (dset, attr_name, memtype, space, H5P_DEFAULT, H5P_DEFAULT);

  //  H5Acreate2( hid_t loc_id, const char *attr_name, hid_t type_id, hid_t space_id, hid_t acpl_id, hid_t aapl_id, ) 
  status = H5Awrite (attr, memtype, attr_value);
  
  status = H5Aclose (attr);
  status = H5Dclose (dset);
  status = H5Sclose (space);
  status = H5Tclose (memtype);
}
void add_attribute_to_dataset( hid_t dset,    char *attr_name, char *attr_value) {
  hid_t         memtype, space,  attr;
  /* Handles */
  herr_t      status;
  hsize_t     dims[1] = {1};
  size_t      sdim;
  
  memtype = H5Tcopy (H5T_C_S1);
  status = H5Tset_size (memtype, H5T_VARIABLE  );
  // status = H5Tset_size (memtype, strlen(attr_value )  );
  status =   H5Tset_cset(memtype, H5T_CSET_UTF8) ;

  
  //   error =       H5Tset_size(datatype_id, "8") ;                   
  
  // space = H5Screate_simple (1, dims, NULL);
  space = H5Screate (H5S_SCALAR);

  attr = H5Acreate (dset, attr_name, memtype, space, H5P_DEFAULT, H5P_DEFAULT);

  //  H5Acreate2( hid_t loc_id, const char *attr_name, hid_t type_id, hid_t space_id, hid_t acpl_id, hid_t aapl_id, ) 
  status = H5Awrite (attr, memtype, &attr_value);
  
  status = H5Aclose (attr);
  status = H5Sclose (space);
  status = H5Tclose (memtype);
}


void add_string_dataset( hid_t file,    char *attr_name, char *attr_value)
{
    hid_t         memtype, space, dset;

    herr_t      status;
    hsize_t     dims[1] = {1};
    size_t      sdim;

    memtype = H5Tcopy (H5T_C_S1);
    status = H5Tset_size (memtype, strlen(attr_value )  );

    // space = H5Screate_simple (1, dims, NULL);
    space = H5Screate (H5S_SCALAR);

    dset = H5Dcreate (file, attr_name,  memtype, space, H5P_DEFAULT);
    status = H5Dwrite (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, attr_value);

    status = H5Dclose (dset);
    status = H5Sclose (space);
    status = H5Tclose (memtype);
}



int create_h5_for_vol(
		  char * vol_name,
		  int num_z,
		  int num_y,
		  int num_x,
		  float voxel_size,
		  float ValMin,
		  float ValMax,
		  float s1,
		  float s2,
		  float S1,
		  float S2
		      )
{
  hid_t       file_id, group_id;                          /* handles */
  hid_t       dataspace, dataset;  
  hid_t       cparms;                     
  
  hsize_t      dims[] = {num_z,num_y, num_x} ;       
  hssize_t     offset[3];
  herr_t       status;                             
  
  
  char target_file_name[  strlen( vol_name) +10  ];
  char * vol_base_name = basename( vol_name ) ;
  sprintf(target_file_name,"%s.h5", vol_name);
  
  /* Create a new file. If file exists its contents will be overwritten. */
  file_id = H5Fcreate ( target_file_name, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  
  
  // dataset = H5Dopen(file_id, "");
  add_attribute_to_dataset( file_id, "NX_class", "NXroot");
  add_attribute_to_dataset( file_id, "file_name", target_file_name);
  add_attribute_to_dataset( file_id, "default", "1.1");

  hid_t group = H5Gcreate(file_id,"1.1",0);
  add_attribute_to_dataset( group, "NX_class", "NXentry");
  add_attribute_to_dataset( group, "default", "volume");

  add_string_dataset(group, "title",  vol_name ) ;
  // status = H5Gclose (group);


  hid_t group_b = H5Gcreate(group,"measurement",0);
  
  add_attribute_to_dataset( group_b, "NX_class", "NXcollection");


  dataspace = H5Screate_simple (3, dims, dims); 
  cparms = H5Pcreate (H5P_DATASET_CREATE);
  printf( " VOLBASE NAME %s\n", vol_base_name);
  status = H5Pset_external (cparms, vol_base_name, 0, dims[0]*dims[1]*dims[3]*sizeof(float));
  dataset = H5Dcreate(group_b, "vol", H5T_NATIVE_FLOAT, dataspace,cparms);

  add_attribute_to_dataset( dataset, "interpretation", "image");

  add_float( group_b,  "voxelSize",voxel_size  ) ;
  add_float( group_b,  "ValMin",  ValMin) ;
  add_float( group_b,  "ValMax",  ValMax) ;
  add_float( group_b,  "s1", s1 ) ;
  add_float( group_b,  "s2", s2 ) ;
  add_float( group_b,  "S1", S1 ) ;
  add_float( group_b,  "S2", S2 ) ;


  hid_t group_c = H5Gcreate(group,"volume",0);
  
  H5Lcreate_soft( "/1.1/measurement/vol" , group_c, "data",  H5P_DEFAULT, H5P_DEFAULT ) ;


  add_attribute_to_dataset( group_c, "NX_class", "NXdata");
  add_attribute_to_dataset( group_c, "signal", "data");

  status = H5Dclose (dataset);
  status = H5Gclose (group_c);
  status = H5Gclose (group_b);
  status = H5Gclose (group);
  
  /* vol.attrs["interpretation"] = u"image" */
  /* for key in ["voxelSize", "ValMin", "ValMax", "s1", "s2", "S1", "S2"]: */
  /*     if key in info: */
  /*         measurement[key] = numpy.float32(info[key]) */
  
  status = H5Fclose (file_id);
}     


