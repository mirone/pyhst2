
#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
int cpu_main(int num_y, int num_x,  float * SLICE, int num_proj, int num_bins, float *WORK_perproje , 
	     float axis_position, float * axis_position_s, float * cos_s, float *sin_s , float cpu_offset_x, float cpu_offset_y,
	     int *minX, int*maxX,
	     int oversampling, int ncpus, float dive,
	     int npj_offset, int *proj_num_list, int numpjs ,
	     int is_helicoidal, int measurerangenotnan,
	     float *angles_per_proj // da usarsi con (is_helicoidal)&&(measurerangenotnan)
	     );

int       pro_cpu_main_conicity( int num_y, int num_x,  float * SLICE, int num_proj, int num_bins, float *data , 
				 float axis_position, float * axis_position_s, float * cos_s, float *sin_s , float cpu_offset_x, float cpu_offset_y,
				 int Nfirstslice, int nslices, int data_start, int nslices_data,
				 float source_distance, float detector_distance, 
				 float v2x ,   float v2z,float voxel_size, 
				 float SOURCE_X , float SOURCE_Z , sem_t sema);



int       cpu_main_conicity( int num_y, int num_x,  float * SLICE, int num_proj, int num_bins, float *WORK_perproje , 
			     float axis_position, float * axis_position_s, float * cos_s, float *sin_s , float cpu_offset_x, float cpu_offset_y,
			     int Nfirstslice, int nslices, int data_start, int nslice_data,
			     float source_distance_v, float detector_distance_v,
			     float v2x ,   float v2z, float v_size, 
			     float SOURCE_X , float SOURCE_Z );
