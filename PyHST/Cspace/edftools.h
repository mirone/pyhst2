
#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

#include<semaphore.h>
#define H5Dopen_vers 1
#include<hdf5.h>



#define LowByteFirst 1
#define HighByteFirst  0
#define UnsignedShort  0
#define FloatValue     1
#define SignedInteger  2
#define UnsignedInteger  3

int byteorder ( void );
char trimma(char  **pos, char **pos2) ;
void    prereadEdfHeader( int *sizeImage , 
			  int *byteorder ,
			  int *datatype  ,
			  long int *hsize     , 
			  int *Dim_1     , 
			  int *Dim_2     , 
			  char *filename,
			  char *CUR_NAME,
			  float *current
			  ) ;
void    prereadEdfHeader_restrained(  int *start_2     , 
				      int *real_2     , 
				      char *filename
				      ) ;
void write_data_to_edf_restrained( float *SLICE, int num_y,int  num_x,
				   int start_y, int real_num_y,
				   const char * nomeout );
void extended_fread(   char *ptr,      /* memory to write in */
                       int   size_of_block,
                       int ndims        ,
                       int *dims      ,
                       int  *strides    ,
                       FILE *stream  ) ;

void     read_data_from_edf( char * filename, 
			     float *dataptr  , // target_float
			     char *buffer  ,   // work buffer
			     int filedatatype, //
			     int rotation_vertical,
			     long int  headerSize,
			     long int pos0 ,  long int pos1 ,
			     long int size0 , long int size1,
			     int sizeofdatatype,
			     int Dim_2,
			     int Dim_1,
			     int filebyteorder,
			     int binning,
			     sem_t * semaforo
			     );


void read_data_from_h5( char * filename, 
			char * dsname,
			int nproj, 
			float *target   , // target_float
			long int pos0 ,  long int pos1 ,
			long int size0 , long int size1,
			int rotation_vertical,
			int binning,
			int multiplo,
			float threshold,
			float *background
			);


void read_projSequence_from_h5( char * filename,
				char * dsname,
				int Ninterval,
				int *nproj_intervals,
				float *target   , // target_float
				long int pos0 ,  long int pos1 ,
				long int size0 , long int size1,
				int rotation_vertical,
				int binning,
				hid_t *file,
				hid_t *dataset,
				hid_t *dataset_current,
				float *bptr
				) ;


void     read_data_from_edf_eli( char * filename, 
				 float *dataptr  , // target_float
				 int filedatatype, //
				 int rotation_vertical,
				 long int  headerSize,
				 long int pos0 ,  long int pos1 ,
				 long int size0 , long int size1,
				 int sizeofdatatype,
				 int Dim_2, int Dim_1,
				 int filebyteorder,
				 int binning,
				 int iread,
				 float DZPERPROJ,
				 float *Ddark,
				 int correct_ff,
				 float *ff_infos,
				 float **FFs,
				 int doubleffcorrection,
				 int takelog,
				 float *ffcorr,
				 float *currents,
				 float curr,
				 sem_t * semaforo
				 );


void read_data_from_h5_eli( char * filename,
			    char * dsname,
			    int nproj,
			    float *target   , // target_float
			    long int pos0 ,  long int pos1 ,
			    long int size0 , long int size1,
			    int rotation_vertical,
			    int binning,
			    float DZPERPROJ,
			    float *Ddark,
			    int correct_ff,
			    float *ff_infos,
			    float **FFs,
			    int doubleffcorrection,
			    int takelog,
			    float *ffcorr,
			    hid_t     *file,      
			    hid_t     *dataset,
			    hid_t *dataset_current
			    ) ;





void write_data_to_edf( float *SLICE, int num_y,int  num_x,  const char * nomeout  ) ;



void write_add_data_to_edf( float *SLICE, int num_y,int  num_x, char * nomeout,
			    int firstline, int nlines) ;

#ifndef EDF_TYPEFUNC_DECLARED
#define EDF_TYPEFUNC_DECLARED
//Typedef for plugins
typedef void (*write_data_to_edf_func)( float *, int ,int , char *) ;
#endif
