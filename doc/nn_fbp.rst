NN-FBP Reconstruction
=====================
  
This reconstruction method  is now available since PyHST2 includes the implementation by Daniel Pelt, Joost Batenburg ( reference to be added)
who gave us the source ( you can see them by cloning the git repository).

You can see a worked out example in the non regressions tests under case NNFBP.

 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: DENOISING_TYPE,ITERATIVE_CORRECTIONS,NNFBP_FILTERS_FILE,NNFBP_NLINEAR,NNFBP_TRAINING_PIXELS_PER_SLICE,NNFBP_TRAINING_RECONSTRUCTION_FILE,NNFBP_TRAINING_USEMASK,NNFBP_TRAINING_MASK_FILE

 
