Geometry
========

  *The following documentation has been generated automatically from the comments found in the code.*

.. automodule:: Parameters_module

.. autoclass:: Parameters
    :members: ROTATION_AXIS_POSITION,NUM_IMAGE_1,NUM_IMAGE_2, ROTATION_VERTICAL, ANGLES_FILE,ANGLE_OFFSET,ANGLE_BETWEEN_PROJECTIONS, START_VOXEL_1, END_VOXEL_1,START_VOXEL_2,END_VOXEL_2,START_VOXEL_3, END_VOXEL_3
