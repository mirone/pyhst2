Patch Fitting
=============

http://arxiv.org/abs/1305.1256

You find the the example in ::

/scisoft/ESRF_sw/TEST_PYHST/MYPHANTOM


.. figure:: datas/examples/patches/slice.png
    :width:  300
    :height: 300
   
    regularised image. 150 projections

.. figure:: datas/examples/patches/patches.png
    :width:  300
    :height: 300
     
    the overcomplete basis

.. figure:: datas/examples/patches/slice_fbp.png
    :width:  300
    :height: 300

    FBP reconstruction

Input File:

.. literalinclude::  datas/examples/patches/input.par
    :language: python

