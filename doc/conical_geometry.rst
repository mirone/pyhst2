Conical Geometry
================


 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members:     CONICITY,CONICITY_FAN,SOURCE_DISTANCE,DETECTOR_DISTANCE,SOURCE_Y,SOURCE_X,STEAM_INVERTER

