Tutorials
===========


Contents:

.. toctree::
   :maxdepth: 2

   tuto_install.rst
   tuto_dfi.rst
   tuto_it_intro.rst
   tuto_it_tv.rst
   tuto_it_dl.rst
   tuto_it_ksvd.rst
   tuto_it_wavelets.rst
   tuto_it_rings.rst
   tuto_it_perfs.rst
   tuto_it_sirtfilter.rst
   tuto_nonregression.rst
   tuto_multipaganin
   tuto_fw_volume_projection.rst
